package cz.cvut.fit.mdw.slaby.uk05.bookings;

import cz.cvut.fit.mdw.slaby.uk05.Config;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;

public class BookingProcessor implements MessageListener {
    private QueueConnectionFactory qconnFactory;

    private QueueConnection qconn;

    private QueueSession qsession;

    private QueueReceiver qbookreceiver;

    private Queue qbook;

    // callback when the message exist in the queue
    public void onMessage(Message msg) {
        try {
            Booking booking = null;
            if (msg instanceof ObjectMessage) {
                booking = (Booking) ((ObjectMessage) msg).getObject();
            } else {
                System.out.println("Received an unknown message!");
            }
            processBooking(booking);
        } catch (JMSException jmse) {
            System.err.println("An exception occurred: " + jmse.getMessage());
        }
    }

    private void processBooking(Booking booking) {
        if (booking == null)
            return;

        System.out.println("Processing new booking for trip: " + booking.getTripId());
        System.out.println("under the name: " + booking.getName());
    }

    // create a connection to the WLS using a JNDI context
    public void init(Context ctx, String inQueue)
            throws NamingException, JMSException {

        qconnFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        qconn = qconnFactory.createQueueConnection();
        qsession = qconn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        qbook = (Queue) ctx.lookup(inQueue);

        qbookreceiver = qsession.createReceiver(qbook);
        qbookreceiver.setMessageListener(this);

        qconn.start();
    }

    // close sender, connection and the session
    public void close() throws JMSException {
        qbookreceiver.close();
        qsession.close();
        qconn.close();
    }

    // start receiving messages from the queue
    public void setup(String inQueue) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ic = new InitialContext(env);

        init(ic, inQueue);

        System.out.println("Connected to " + qbook.toString() + ", receiving bookings...");
        try {
            synchronized (this) {
                while (true) {
                    this.wait();
                }
            }
        } finally {
            close();
            System.out.println("Finished.");
        }
    }

    public static void main(String[] args) throws Exception {
        // input arguments
        String inQueue = "jms/mdw-booking-queue";

        // create the producer object and setup the message
        BookingProcessor bookingProcessor = new BookingProcessor();
        bookingProcessor.setup(inQueue);
    }
}
