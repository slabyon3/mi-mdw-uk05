package cz.cvut.fit.mdw.slaby.uk05.bookings;

import java.io.Serializable;

public class Booking implements Serializable {
    private int tripId;
    private String name;

    public Booking(int tripId, String name) {
        this.tripId = tripId;
        this.name = name;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
