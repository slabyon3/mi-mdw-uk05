package cz.cvut.fit.mdw.slaby.uk05.orders;

import cz.cvut.fit.mdw.slaby.uk05.Config;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;

public class OrderProcessor implements MessageListener {
    private QueueConnectionFactory qconnFactory;

    private QueueConnection qconn;

    private QueueSession qsession;

    private QueueReceiver qordreceiver;
    private QueueSender qtripsender;
    private QueueSender qbooksender;

    private Queue qreq;
    private Queue qtrip;
    private Queue qbook;

    // callback when the message exist in the queue
    public void onMessage(Message msg) {
        try {
            Order order = null;
            if (msg instanceof ObjectMessage) {
                order = (Order) ((ObjectMessage) msg).getObject();
            } else {
                System.out.println("Unknown message received!");
            }
            routeOrder(order);
        } catch (JMSException jmse) {
            System.err.println("An exception occurred: " + jmse.getMessage());
        }
    }

    private void routeOrder(Order order) throws JMSException {
        if (order == null)
            return;
        if (order.getTrip() != null) {
            System.out.println("Received new trip order, forwarding to " + qtrip.toString());
            ObjectMessage msg = qsession.createObjectMessage(order.getTrip());
            qtripsender.send(msg, DeliveryMode.PERSISTENT, 8, 0);
        }
        if (order.getBooking() != null) {
            System.out.println("Received new booking order, forwarding to " + qbook.toString());
            ObjectMessage msg = qsession.createObjectMessage(order.getBooking());
            qbooksender.send(msg, DeliveryMode.PERSISTENT, 8, 0);
        }
    }

    // create a connection to the WLS using a JNDI context
    public void init(Context ctx, String inQueue, String bookQueue, String tripQueue)
            throws NamingException, JMSException {

        qconnFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        qconn = qconnFactory.createQueueConnection();
        qsession = qconn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        qreq = (Queue) ctx.lookup(inQueue);
        qbook = (Queue) ctx.lookup(bookQueue);
        qtrip = (Queue) ctx.lookup(tripQueue);

        qordreceiver = qsession.createReceiver(qreq);
        qordreceiver.setMessageListener(this);

        qbooksender = qsession.createSender(qbook);
        qtripsender = qsession.createSender(qtrip);

        qconn.start();
    }

    // close sender, connection and the session
    public void close() throws JMSException {
        qordreceiver.close();
        qtripsender.close();
        qbooksender.close();
        qsession.close();
        qconn.close();
    }

    // start receiving messages from the queue
    public void setup(String inQueue, String bookQueue, String tripQueue) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ic = new InitialContext(env);

        init(ic, inQueue, bookQueue, tripQueue);

        System.out.println("Connected to " + qreq.toString() + ", receiving messages...");
        System.out.println("Will use " + qbook.toString() + " to send bookings...");
        System.out.println("Will use " + qtrip.toString() + " to send trips...");

        try {
            synchronized (this) {
                while (true) {
                    this.wait();
                }
            }
        } finally {
            close();
            System.out.println("Finished.");
        }
    }

    public static void main(String[] args) throws Exception {
        // input arguments
        String bookQueue = "jms/mdw-booking-queue";
        String tripQueue = "jms/mdw-trip-queue";
        String inQueue = "jms/mdw-order-queue";

        // create the producer object and setup the message
        OrderProcessor bookingProcessor = new OrderProcessor();
        bookingProcessor.setup(inQueue, bookQueue, tripQueue);
    }
}
