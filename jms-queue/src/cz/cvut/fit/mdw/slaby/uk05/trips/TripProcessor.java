package cz.cvut.fit.mdw.slaby.uk05.trips;

import cz.cvut.fit.mdw.slaby.uk05.Config;
import cz.cvut.fit.mdw.slaby.uk05.bookings.Booking;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;

public class TripProcessor implements MessageListener {
    private QueueConnectionFactory qconnFactory;

    private QueueConnection qconn;

    private QueueSession qsession;

    private QueueReceiver qtripreceiver;

    private Queue qtrip;

    // callback when the message exist in the queue
    public void onMessage(Message msg) {
        try {
            Trip trip = null;
            if (msg instanceof ObjectMessage) {
                trip = (Trip) ((ObjectMessage) msg).getObject();
            } else {
                System.out.println("Received an unknown message!");
            }
            processTrip(trip);
        } catch (JMSException jmse) {
            System.err.println("An exception occurred: " + jmse.getMessage());
        }
    }

    private void processTrip(Trip trip) {
        if (trip == null)
            return;

        System.out.println("Processing new trip with name: " + trip.getName());
        System.out.println("and capacity: " + trip.getCapacity());
    }

    // create a connection to the WLS using a JNDI context
    public void init(Context ctx, String inQueue)
            throws NamingException, JMSException {

        qconnFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        qconn = qconnFactory.createQueueConnection();
        qsession = qconn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        qtrip = (Queue) ctx.lookup(inQueue);

        qtripreceiver = qsession.createReceiver(qtrip);
        qtripreceiver.setMessageListener(this);

        qconn.start();
    }

    // close sender, connection and the session
    public void close() throws JMSException {
        qtripreceiver.close();
        qsession.close();
        qconn.close();
    }

    // start receiving messages from the queue
    public void setup(String inQueue) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ic = new InitialContext(env);

        init(ic, inQueue);

        System.out.println("Connected to " + qtrip.toString() + ", receiving trips...");
        try {
            synchronized (this) {
                while (true) {
                    this.wait();
                }
            }
        } finally {
            close();
            System.out.println("Finished.");
        }
    }

    public static void main(String[] args) throws Exception {
        // input arguments
        String inQueue = "jms/mdw-trip-queue";

        // create the producer object and setup the message
        TripProcessor bookingProcessor = new TripProcessor();
        bookingProcessor.setup(inQueue);
    }
}
