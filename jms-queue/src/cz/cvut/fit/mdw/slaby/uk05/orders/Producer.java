package cz.cvut.fit.mdw.slaby.uk05.orders;

import cz.cvut.fit.mdw.slaby.uk05.Config;
import cz.cvut.fit.mdw.slaby.uk05.bookings.Booking;
import cz.cvut.fit.mdw.slaby.uk05.trips.Trip;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Producer{
    private QueueConnectionFactory qconnFactory;

    private QueueConnection qconn;
    private QueueSession qsession;

    private QueueSender qreqsender;

    private Queue qreq;

    // create a connection to the WLS using a JNDI context
    public void init(Context ctx, String outQueue)
            throws NamingException, JMSException {
        qconnFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        qconn = qconnFactory.createQueueConnection();

        qsession = qconn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

        qreq = (Queue) ctx.lookup(outQueue);
        qreqsender = qsession.createSender(qreq);

        qconn.start();
    }

    // close sender, connection and the session
    public void close() throws JMSException {
        qreqsender.close();
        qsession.close();
        qconn.close();
    }

    // sends the message to the queue
    public void setup(String outQueue) throws Exception {
        // create a JNDI context to lookup JNDI objects (connection factory and queue)
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ic = new InitialContext(env);
        init(ic, outQueue);

        System.out.println("Will use " + qreq.toString() + " to send orders...");
    }

    public static void main(String[] args) throws Exception {
        // input arguments
        String msg = "Hello";
        String outQueue = "jms/mdw-order-queue";

        Producer producer = new Producer();
        producer.setup(outQueue);
        Scanner scanner = new Scanner(System.in);
        boolean active = true;
        while (active) {
            String request = scanner.next();

            switch (request) {
                case "quit":
                    producer.close();
                    active = false;
                    break;
                case "booking":
                    producer.sendBooking(scanner);
                    break;
                case "trip":
                    producer.sendTrip(scanner);
                    break;
                default:
                    System.out.println("Invalid command!");
            }
        }
    }

    private void sendTrip(Scanner scanner) {
        int capacity;
        String name;

        try {
            System.out.print("Please enter the new trip's capacity: ");
            capacity = scanner.nextInt();
            System.out.print("Please enter the new trip's name: ");
            scanner.nextLine();
            name = scanner.nextLine();
        } catch (NoSuchElementException e) {
            System.out.println("Error reading your input! Please try again...");
            return;
        }

        Order order = new Order(
                new Trip(name, capacity),
                null
        );

        sendOrder(order);
    }

    private void sendOrder(Order order) {
        try {
            ObjectMessage msg = qsession.createObjectMessage(order);
            qreqsender.send(msg, DeliveryMode.PERSISTENT, 8, 0);
            System.out.println("The message was sent to the destination " +
                    qreqsender.getDestination().toString());
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    private void sendBooking(Scanner scanner) {
        int tripId;
        String name;

        try {
            System.out.print("Please enter your desired trip ID: ");
            tripId = scanner.nextInt();
            System.out.print("Please enter your name: ");
            scanner.nextLine();
            name = scanner.nextLine();
        } catch (NoSuchElementException e) {
            System.out.println("Error reading your input! Please try again...");
            return;
        }

        Order order = new Order(
                null,
                new Booking(tripId, name)
        );

        sendOrder(order);
    }
}
