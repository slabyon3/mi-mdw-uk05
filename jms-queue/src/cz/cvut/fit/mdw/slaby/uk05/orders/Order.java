package cz.cvut.fit.mdw.slaby.uk05.orders;

import cz.cvut.fit.mdw.slaby.uk05.bookings.Booking;
import cz.cvut.fit.mdw.slaby.uk05.trips.Trip;

import java.io.Serializable;

public class Order implements Serializable {
    private Trip trip;
    private Booking booking;

    public Order(Trip trip, Booking booking) {
        this.trip = trip;
        this.booking = booking;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }
}
